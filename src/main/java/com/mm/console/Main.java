package com.mm.console;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mm.client.IssClient;
import com.mm.controller.ConsoleController;
import com.mm.service.IssService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void run() {

        var scanner = new Scanner();

        IssClient issClient = new IssClient();
        ObjectMapper objectMapper = new ObjectMapper();
        IssService issService = new IssService(issClient, objectMapper);
        ConsoleController consoleController = new ConsoleController(issService);

        while (true) {
            Menu.printMainMenu();
            int input = scanner.loadUserInput();
            if (input == 0) {
                break;
            }

            switch (input) {
                case 1:
                    LOGGER.info("{}", consoleController.getCurrentPosition());
                    break;
                case 2:
                    LOGGER.info("{}", consoleController.getISSSpeed());
                    break;
                case 3:
                    LOGGER.info("{}", consoleController.getListOfPeople());
                    break;
                default:
                    LOGGER.info("This option ({}) is not defined", input);
                    break;
            }
        }
    }
}
