package com.mm.console;

public class Scanner {

    public int loadUserInput() {
        var scanner = new java.util.Scanner(System.in);
        var s = scanner.nextLine();

        return Integer.parseInt(s);
    }
}
