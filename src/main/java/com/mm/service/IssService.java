package com.mm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mm.Application;
import com.mm.client.IssClient;
import com.mm.dto.Coordinates;
import com.mm.dto.People;
import com.mm.model.Person;
import com.mm.model.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class IssService {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
    private static final String ASTROS_URI = "http://api.open-notify.org/astros.json";
    private static final String ISS_PLACE = "http://api.open-notify.org/iss-now.json";
    private final ObjectMapper objectMapper;

    private final IssClient issClient;

    public IssService(IssClient issClient, ObjectMapper  objectMapper) {
        this.issClient = issClient;
        this.objectMapper = objectMapper;
    }


    public List<Person> getAstronauts() {
        String stringAstronauts =  issClient.get(ASTROS_URI);

        return deserializeAstronauts(stringAstronauts);
    }

    private List<Person> deserializeAstronauts (String json) {

        try {
            People people = objectMapper.readValue(json, new TypeReference<People>() {
            });

            return people.getPeople();

        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to deserialize", e);
            return List.of();
        }
    }


    public Coordinates getIssPosition() {
        String location =  issClient.get(ISS_PLACE);

        return deserializePosition(location);
    }

    private Coordinates deserializePosition (String json) {

        try {

            Position position = objectMapper.readValue(json, new TypeReference<Position>() {
            });

            return position.getIss_position();

        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to deserialize", e);
//            return List.of();
            return null;
        }




//
//        try {
//            Collection<Position> position = objectMapper.readValue(json, new TypeReference<Collection<Position>>() {
//            });
//
//            List<Coordinates> positions = null;
//            for (Position position1 : position) {
//                positions.addAll(position1.getIss_position());
//            }
//
//            return positions;
//
//        } catch (JsonProcessingException e) {
//            LOGGER.error("Failed to deserialize", e);
//            return List.of();
////            return null;
//        }






    }

}
