package com.mm.dto;

import com.mm.model.Person;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class People {

    private List<Person> people;
    private int number;
    private String message;
}
