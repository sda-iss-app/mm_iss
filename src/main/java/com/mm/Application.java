package com.mm;
import com.mm.console.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;


public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

//        LOGGER.info("Starting ISS APP...");
        Main.run();


    }
}
