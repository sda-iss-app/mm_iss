package com.mm.controller;

import com.mm.model.Person;
import com.mm.service.IssService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ConsoleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleController.class);
    private IssService issService;

    public ConsoleController(IssService issService) {
        this.issService = issService;
    }

    public String getCurrentPosition() {

        LOGGER.info("Showing current ISS position");

        return issService.getIssPosition().toString();
    }

    public float getISSSpeed() {
        LOGGER.info("Get current speed");

        return 0f;
    }

    public List<Person> getListOfPeople() {
        LOGGER.info("Showing Astronauts");

        return issService.getAstronauts();
    }
}
