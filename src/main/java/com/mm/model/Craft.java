package com.mm.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="craft")
@Data
public class Craft {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
}
