package com.mm.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="person_in_measurement")
@Data
public class PersonInMeasurement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

}
