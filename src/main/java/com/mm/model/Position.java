package com.mm.model;

import com.mm.dto.Coordinates;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Position {

    Coordinates iss_position;
    double timestamp;
    String message;

}
