package com.mm.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="position_measure")
@Data
public class PositionMeasure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Timestamp measure_time;
    Double latitude;
    Double longitude;
    Timestamp create_at;

}
