package com.mm.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="iss_speed")
@Data
public class ISSSpeed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Long speed;

    @Column(name="order_date")
    LocalDate orderDate;

}
